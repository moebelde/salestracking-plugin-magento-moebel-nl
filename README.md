Magento 2 Salestracking by meubelo
===========================================

[![Latest Stable Version]](https://packagist.org/packages/moebelde/module-meubelo-salestracking)
[![Total Downloads]](https://packagist.org/packages/moebelde/module-meubelo-salestracking)

## How to install moebel sales tracking module

### 1. Install via composer (recommend)

We recommend you to install moebelde/module-meubelo-salestracking module via composer. It is easy to install, update and maintaince.

Run the following command in Magento 2 root folder.

#### 1.1 Install

```
composer require moebelde/module-meubelo-salestracking
php bin/magento setup:upgrade
bin/magento module:enable Meubelo_Salestracking
php bin/magento setup:di:compile
bin/magento cache:clean
```


#### Support
v1.0.0 - Magento 2.2.*, 2.3.*, 2.4.*